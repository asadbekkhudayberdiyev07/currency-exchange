
import { useEffect, useState } from 'react';
import './App.css';
import useRequest from './hooks/useRequest/useRequest';
import Converter from './components/converter';
import Currencies from './components/currencies';

function App() {
  const [client, data] = useRequest()
  const [active, setActive] = useState("con")

  const changeActive = (name) => {
    setActive(name)
  }
  
  return (
    <div className="App">
      <div className='tabs'>
        <div className="tab-header">
          <div className={`item ${active == "con" ? "active" : ""}`} onClick={() => changeActive("con")}>Converter</div>
          <div className={`item ${active == "cur" ? "active" : ""}`} onClick={() => changeActive("cur")}>Currencies</div>
        </div>
        <div className="tab-content">
        {active === "con" ? <Converter/> : <Currencies/>}
        </div>
      </div>
    </div>
  );
}

export default App;
