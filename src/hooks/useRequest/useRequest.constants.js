export const REQUEST_STATUSES = {
    initial: "INITIAL",
    success: "SUCCESS",
    failed: "FAILED",
    loading: "LOADING"
}