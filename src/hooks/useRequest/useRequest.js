import { useState } from "react";

import api from "../../services/api/api";

const useRequest = () => {
    const client = api()
    const apiKey=process.env.REACT_APP_API_KEY
    const [data, setData] = useState()

    const get = async (url) => {
        await sendRequest("get", `/${url}&api_key=${apiKey}`)
    }
    const sendRequest = async (method, url, data) => {
        try{
            const res = await client[method](url, data)
            setData(res?.data)
            return res;
        } catch (err){

        }
    }
    return [{get}, data];
}

export default useRequest;