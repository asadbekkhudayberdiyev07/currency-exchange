import axios from "axios";

const api = () => {
    const baseURL = process.env.REACT_APP_API_URL
    const client = axios.create({
        baseURL,
        timeout: 5000,
        headers: {
            'Content-Type': 'application/json'
        }
    })
    return client
}

export default api;