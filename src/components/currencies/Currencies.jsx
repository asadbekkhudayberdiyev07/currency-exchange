import { useEffect, useState } from "react";
import { currencies } from "../../constants/currencies";
import "./currencies.css"
import useRequest from "../../hooks/useRequest/useRequest";

const Currencies = () => {
    const [fromCurrency, setFromCurrency] = useState(currencies[0])
    const [client, data] = useRequest() 

    const getAmount = () => {
        client.get(`fetch-multi?from=${fromCurrency}&to=${currencies?.toString()}`)
    }

    useEffect(() => {
        getAmount()
    }, [fromCurrency])

    return <>
        <label for="currency">Base currency:</label>
        <br/>
        <select name="currency" id="currency" onChange={(e) => setFromCurrency(e?.target?.value)}>
            {currencies?.map((item) => {
                return <option value={item}>{item}</option>
            })}
        </select>
        <div className="info">       
            {
                data ? Object.keys(data?.results)?.map((item, index) => {
                    return <div className="info-item">
                        1 {fromCurrency} = {Object.values(data?.results)?.find((item1, index1) => index1 === index)} {item}
                    </div>
                }) : ""
            }
         </div>
    </>;
}

export default Currencies;